#MAC0110 - MiniEP7
#Gustavo Korzune Gurgel - 4778350

#PARTE 1

function seno(x)
    sen = zero(BigFloat)
    for i = 0:9
        sen += (((-1)^i)*x^(2*i+1))/(factorial(2*i+1))
    end
    return sen
end

function cosseno(x)
    cos = zero(BigFloat)
    for i = 0:9
        cos += ((-1)^i*x^(2*i))/(factorial(2*i))
    end
    return cos
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tangente(x)
    tangente = zero(BigFloat)
    for i = 1:10
        tangente += (2^(2*i)*(2^(2*i)-1)*bernoulli(i)*x^(2*i-1))/(factorial(2*i))
    end
    return tangente
end


#PARTE 2

function quaseigual(v1,v2) #função para três casas decimais
    δ = 1e-3
    return (abs(big(v1)-big(v2)) < δ)
end

function check_sin(value,x)
    return quaseigual(value, seno(x))
end

function check_cos(value,x)
    return quaseigual(value, cosseno(x))
end

function check_tan(value,x)
    return quaseigual(value, tangente(x))
end
